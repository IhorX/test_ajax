let users = []

function getUsers() {
    return fetch('https://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then(resUsers => resUsers)
}

function renderUsers(users) {
    const ul = document.querySelector('.list')
    ul.innerHTML = ''

    for (const user of users) {
        const li = document.createElement('li')
        li.classList.add('list-group-item')
        li.innerText = user.name + ' ' + user.email + ' ' + user.phone + ' ' + user.website
        ul.append(li)
    }
}

getUsers()
    .then(u => {
        users = u
        renderUsers(u)
    })

const input = document.querySelector('#exampleInputEmail1')

input.addEventListener('input', function (event) {
    const filterText = event.target.value

    renderUsers(users.filter(u => u.name.includes(filterText) + u.email.includes(filterText) + u.phone.includes(filterText) + u.website.includes(filterText)))})


